use rand::{thread_rng, Rng};
use std::fmt;
use std::io::{self, Write};

pub enum Numbers {
    Ichi = 1,
    Ni,
    San,
    Shi,
    Go,
    Roku,
}

impl std::fmt::Display for Numbers {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        let num = match self {
            Self::Ichi => "Ichi",
            Self::Ni => "Ni",
            Self::San => "San",
            Self::Shi => "Shi",
            Self::Go => "Go",
            Self::Roku => "Roku",
        };

        write!(f, "{}", num)
    }
}

#[derive(PartialEq, Eq)]
enum Chohan {
    Cho,
    Han,
    Err,
}

pub fn banner(msg: &str) {
    println!(
        "----------------------
{msg}
By Al Sweigart al@inventwithpython.com
(rust-ed 🦀 by lij)
----------------------\n"
    );
}

pub fn instructions() {
    println!(
        "In this traditional Japanese dice game, two dice are rolled in a bamboo
cup by the dealer sitting on the floor. The player must guess if the
dice total to an even (cho) or odd (han) number.\n"
    )
}

pub fn build_tension() {
    println!(
        "\nThe dealer swirls the cup and you hear the rattle of dice. 
The dealer slams the cup on the floor, 
still covering the dice and asks for your bet."
    )
}

pub fn get_input(msg: &str) -> String {
    println!("{msg}");
    print!("> ");
    let mut raw = String::new();
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut raw)
        .expect("Failed to read line");

    raw.trim().to_string()
}

pub fn get_bet(min: u32, max: u32) -> u32 {
    let mut bet: u32;
    loop {
        let raw = get_input(&format!("How much do you bet? (${min}-${max}, or QUIT):"));
        if raw == "QUIT" {
            return 0;
        }
        bet = match raw.parse() {
            Ok(n) => n,
            Err(_) => max + 10,
        };
        if bet > min && bet <= max {
            break;
        } else {
            println!("🙄 ... Your bet must be a number between ${min} and ${max}");
        }
    }
    bet
}

fn get_choice() -> Chohan {
    let mut choice: Chohan;
    loop {
        let mut raw = get_input("\nCHO (even) or HAN (odd)?");
        raw.make_ascii_lowercase();
        choice = match raw.trim() {
            "cho" => Chohan::Cho,
            "han" => Chohan::Han,
            &_ => Chohan::Err,
        };
        if choice != Chohan::Err {
            break;
        } else {
            println!("Please enter either \"CHO\" or \"HAN\".");
        }
    }
    choice
}

fn roll_dice() -> Numbers {
    let mut rng = thread_rng();
    match rng.gen_range(1..7) {
        1 => Numbers::Ichi,
        2 => Numbers::Ni,
        3 => Numbers::San,
        4 => Numbers::Shi,
        5 => Numbers::Go,
        _ => Numbers::Roku,
    }
}

pub fn play(money: &mut u32, bet: &u32) {
    let d1 = roll_dice();
    let d2 = roll_dice();

    let choice = get_choice();

    println!("\nThe dealer lifts the cup to reveal:");
    println!("\t{} - {}", d1, d2);
    let d1_value = d1 as u32;
    let d2_value = d2 as u32;
    println!("\t{} - {}\n", d1_value, d2_value);

    let result = (d1_value + d2_value) % 2;
    let cut = bet / 10;
    if (result == 0 && choice == Chohan::Cho) || (result == 1 && choice == Chohan::Han) {
        println!("You won! You take ${}.", bet - cut);
        println!("The house collects a ${} fee.\n", cut);
        *money += bet - cut;
    } else {
        println!("You lost ${}.\n", bet);
        *money -= bet;
    }
}
