fn main() {
    chohan::banner("🎲 Cho-Han 🎲");
    chohan::instructions();

    const MIN_BET: u32 = 10;
    const INITIAL_MONEY: u32 = 5000;
    let mut money = INITIAL_MONEY;
    loop {
        println!("Currently you have ${money}. Minimum bet is ${MIN_BET}.");
        if money < MIN_BET {
            println!("You don't have enough money to bet. That is all. Thanks for playing.");
            break;
        };
        let bet = chohan::get_bet(MIN_BET, money);
        if bet == 0 {
            println!("\nThanks for playing. You ended with ${money}");
            break;
        } else {
            chohan::build_tension();
            chohan::play(&mut money, &bet);
        }
    }
}
